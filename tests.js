const { expect } = require('chai')
const { bufferPadStart, bufferPadEnd } = require('./index')

describe('testing bufferPadStart', () => {
	it('should add padding when buffer is too small', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadStart(input, 5)
		const expected = Buffer.from([0, 0, 1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('padding should be made of given byte', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadStart(input, 5, 42)
		const expected = Buffer.from([42, 42, 1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('should do nothing when buffer has max length', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadStart(input, 3)
		const expected = Buffer.from([1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('should do nothing when buffer is too big', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadStart(input, 1)
		const expected = Buffer.from([1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})
})

describe('testing bufferPadEnd', () => {
	it('should add padding when buffer is too small', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadEnd(input, 5)
		const expected = Buffer.from([1, 2, 3, 0, 0])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('padding should be made of given byte', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadEnd(input, 5, 42)
		const expected = Buffer.from([1, 2, 3, 42, 42])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('should do nothing when buffer has max length', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadEnd(input, 3)
		const expected = Buffer.from([1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})

	it('should do nothing when buffer is too big', function () {
		const input = Buffer.from([1, 2, 3])
		const output = bufferPadEnd(input, 1)
		const expected = Buffer.from([1, 2, 3])
		expect(output).to.deep.equal(expected)
		expect(input).to.not.equal(output)
	})
})
