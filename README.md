# Pad Buffer

This package provides two function (`bufferPadStart` and `bufferPadEnd`).
They can be used to add padding to a buffer.
By default, the padding consists of null bytes, but you can choose a custom filling value.

## Usage

### Importing the functions

```js
const { bufferPadStart, bufferPadEnd } = require('pad-buffer')
```

or

```ts
import { bufferPadStart, bufferPadEnd } from 'pad-buffer'
```

### Using the functions

```js
// <Buffer 01 02 03>
const buffer = Buffer.from([1, 2, 3])

// <Buffer 00 00 01 02 03>
const padStartResult = bufferPadStart(buffer, 5)
// <Buffer 01 02 03 00 00>
const padEndResult = bufferPadEnd(buffer, 5)

// <Buffer 09 09 01 02 03>
const customPaddingResult = bufferPadStart(buffer, 5, 9)
```
