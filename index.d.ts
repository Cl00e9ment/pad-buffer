declare module 'pad-buffer' {
	/**
	 * Copies the given buffer and pads the copy with a given byte (possibly repeated) so that the resulting buffer reaches a given length.
	 * The padding is applied from the start (left) of the buffer copy.
	 *
	 * @param {Buffer} buffer The buffer to pad. It will not be modified, a copy will be created.
	 * @param {number} maxLength The length of the resulting buffer once the given buffer has been padded.
	 * If this parameter is smaller than the given buffer's length, the copy of the buffer will be returned without modification.
	 * @param {number} [fillByte=0] The byte to pad the given buffer with. The default value for this parameter is 0.
	 * @return {Buffer} A copy of the given buffer with the applied padding.
	 */
	export function bufferPadStart(buffer: Buffer, maxLength: number, fillByte?: number): Buffer

	/**
	 * Copies the given buffer and pads the copy with a given byte (possibly repeated) so that the resulting buffer reaches a given length.
	 * The padding is applied from the end (right) of the buffer copy.
	 *
	 * @param {Buffer} buffer The buffer to pad. It will not be modified, a copy will be created.
	 * @param {number} maxLength The length of the resulting buffer once the given buffer has been padded.
	 * If this parameter is smaller than the given buffer's length, the copy of the buffer will be returned without modification.
	 * @param {number} [fillByte=0] The byte to pad the given buffer with. The default value for this parameter is 0.
	 * @return {Buffer} A copy of the given buffer with the applied padding.
	 */
	export function bufferPadEnd(buffer: Buffer, maxLength: number, fillByte?: number): Buffer
}
